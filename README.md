Hide messages in image files by modifying the last 2 bits in every color channel. [Computerphile] video as a reference.

# Requirements
[Python]

# Usage
There are 2 ways:
- ```sh
    python %job% %type% %imagename% %filename%
    ```
    (where job=encode/decode and type=text/file, type is used for encoding a textfile or a typed string. depending on type, filename could be string or filename.txt).
    
- Use START.bat for a quick start.

Make sure to have your secret.txt and image in the same directory. Otherwise, you would need to type absolute path.



[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen.)


   [Python]: <https://www.python.org/downloads/>
   [Computerphile]: <https://www.youtube.com/watch?v=TWEXCYQKyDc/>