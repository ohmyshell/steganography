@echo off
cls

:asciiART
echo      ___ _                                              _        
echo     ^/ __^| ^|_ ___ __ _ __ _ _ _  ___  __ _ _ _ __ _ _ __^| ^|_ _  _ 
echo     ^\__ ^\  _^/ -_) _` ^/ _` ^| ' ^\^/ _ ^\^/ _` ^| '_^/ _` ^| '_ ^\ ' ^\ ^|^| ^|
echo     ^|___^/^\__^\___^\__, ^\__,_^|_^|^|_^\___^/^\__, ^|_^| ^\__,_^| .__^/_^|^|_^\_, ^|
echo                 ^|___^/               ^|___^/         ^|_^|       ^|__^/ 

setlocal

:ask
    set /p answer=Do you want to encode/decode?[e/d]  
    if %answer% equ e (
        set job=encode
        set /p imagename=Please Enter Image Name with extension ^(for example: image.jpg^)  
        goto :infoEncode
    ) else (
        if %answer% equ d (
            set job=decode
            set /p imagename=Please Enter Image Name with extension ^(for example: encoded.png^)  
            goto :dojob
        ) else (
            echo Please type e for encode or d for decode
            goto :ask
        )
    )

:infoEncode
    set /p type=Do you want to encode textfile or enter text?[f/t] 
    if %type% equ f (
        set type=file
        set /p filename=Please enter filename with extension ^(only txt files supported, for example, secret.txt^)  
        goto :dojob
    ) else (
        if %type% equ t (
            set type=text
            set /p filename=Enter text you want to encode: 
            goto :dojob
        ) else (
            echo Please type f for textfile or t for text
            goto :infoEncode
        )
    )

:dojob
    if %job% equ decode (
        python steganography.py %job% %imagename% > output.txt
        echo Please check output.txt for message.
    ) else (
        python steganography.py %job% %type% %imagename% "%filename%"
        echo Done
    )
endlocal
pause
goto :eof

